//
//  SecondViewController.swift
//  WeatherTest
//
//  Created by Luka on 22/02/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import CoreLocation
import Moya
import Kingfisher

class ForecastVCLuka: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    let locationManager = CLLocationManager()
    var lat:Double = 0
    var lon:Double = 0
    let currentDate = Date()
    let dateFormatter = DateFormatter()

    
    override func viewWillAppear(_ animated: Bool) {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        moyaCall()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.sectionHeaderHeight = 40
    }
    
    private var weatherList : [List]
        = []
    
    var groupedWeatherList = [[List]]()
 
    
    func moyaCall(){
        print(lon)
        print(lat)
        let provider = MoyaProvider<WeatherApp>()
        provider.request(.weather(lat,lon)) { result in
            
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data // Data, your JSON response is probably in here!
                let statusCode = moyaResponse.statusCode // Int - 200, 401, 500, etc
                let decoded = try? JSONDecoder().decode(Welcome.self, from: data)
                if let welcomen : Welcome = decoded{
                DispatchQueue.main.async {
                    self.weatherList=welcomen.list
                    self.groupWeather()
                    self.tableView.reloadData()
                   
                    
                }
                }
                
            // do something in your app
            case let .failure(error):
                // TODO: handle the error == best. comment. ever.
                print(error)
            }
            
        }
        
    }
    
    func groupWeather(){
        if self.weatherList.count > 0 {
            var comparingWeather = weatherList[0]
            self.groupedWeatherList.append([comparingWeather])
            
            for i in 1 ..< weatherList.count {
                let weather = weatherList[i]
                if Calendar.current.isDate(weather.getDate(), inSameDayAs: comparingWeather.getDate()) {
                    self.groupedWeatherList[self.groupedWeatherList.count - 1].append(weather)
                } else {
                    self.groupedWeatherList.append([weather])
                    comparingWeather = weather
                }
            }
        } else {
            self.groupedWeatherList = []
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.groupedWeatherList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupedWeatherList[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let date = self.groupedWeatherList[section][0].getDate()
        if Calendar.current.isDate(date, inSameDayAs: currentDate) {
            return "Today"
        }
        let title = dateFormatter.weekdaySymbols[Calendar.current.component(.weekday, from: date)-1]
        return title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell") as? WeatherCell {
            let new = self.groupedWeatherList[indexPath.section][indexPath.row]
            cell.updateViews(weather: new)
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.backgroundColor = .clear
            tableView.layer.backgroundColor = UIColor.clear.cgColor
            tableView.backgroundColor = .clear
            return cell
        } else {
            return WeatherCell()
        }
    }
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        lat=locValue.latitude
        lon=locValue.longitude
        moyaCall()
    }
    
    


}

