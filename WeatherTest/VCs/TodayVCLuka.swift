//
//  FirstViewController.swift
//  WeatherTest
//
//  Created by Luka on 22/02/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Moya
import CoreLocation
import Kingfisher

class TodayVCLuka: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var weatherImg: UIImageView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var weatherLbl: UILabel!
    @IBOutlet weak var rainChance: UILabel!
    @IBOutlet weak var precipitation: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var windSpd: UILabel!
    @IBOutlet weak var compass: UILabel!
    
    let locationManager = CLLocationManager()
    var lat:Double = 0
    var lon:Double = 0
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.title = "Today"
        getLocation()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.navigationItem.title = locationLbl.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
    }
    
    func moyaCall(){
        print(lon)
        print(lat)
        let provider = MoyaProvider<WeatherApp>()
        provider.request(.weather(lat,lon)) { result in

            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data // Data, your JSON response is probably in here!
                let statusCode = moyaResponse.statusCode // Int - 200, 401, 500, etc
                if let decoded = try? JSONDecoder().decode(Welcome.self, from: data){
                DispatchQueue.main.async {
                    self.locationLbl.text=decoded.city.name
                    self.weatherLbl.text = String(format: "%.0f", ((decoded.list.first?.main.temp)!-273.15).rounded()) + "°C | " + (decoded.list.first?.weather.first?.description)!
                    self.rainChance.text = String((decoded.list.first?.main.humidity) ?? 0) + "%"
                    self.precipitation.text = String((decoded.list.first?.rain?.the3H) ?? 0) + "mm"
                    self.windSpd.text =  String(format: "%.1f", (decoded.list.first?.wind.speed ?? 0)*18/5) + "km/h"
                    
                    self.pressure.text = String(format: "%.0f", (decoded.list.first?.main.pressure)!.rounded()) + " hPa"
                    
                    
                    let url = URL(string:"http://openweathermap.org/img/w/" + (decoded.list.first?.weather.first?.icon)! + ".png")!
                    self.weatherImg.kf.setImage(with: url)
                    
                    let degrees:Double = (decoded.list.first?.wind.deg)!
                    
                    if 348.75 <= degrees, degrees <= 360 {
                        self.compass.text = "N"
                    } else if 0 <= degrees,degrees <= 11.25 {
                        self.compass.text = "N"
                    } else if 11.25 < degrees, degrees <= 33.75 {
                        self.compass.text = "NNE"
                    } else if 33.75 < degrees, degrees <= 56.25 {
                        self.compass.text = "NE"
                    } else if 56.25 < degrees, degrees <= 78.75 {
                        self.compass.text = "ENE"
                    } else if 78.75 < degrees, degrees <= 101.25 {
                        self.compass.text = "E"
                    } else if 101.25 < degrees, degrees <= 123.75 {
                        self.compass.text = "ESE"
                    } else if 123.75 < degrees, degrees <= 146.25 {
                        self.compass.text = "SE"
                    } else if 146.25 < degrees, degrees <= 168.75 {
                        self.compass.text = "SSE"
                    } else if 168.75 < degrees, degrees <= 191.25 {
                        self.compass.text = "S"
                    } else if 191.25 < degrees, degrees <= 213.75 {
                        self.compass.text = "SSW"
                    } else if 213.75 < degrees, degrees <= 236.25 {
                        self.compass.text = "SW"
                    } else if 236.25 < degrees, degrees <= 258.75 {
                        self.compass.text = "WSW"
                    } else if 258.75 < degrees, degrees <= 281.25 {
                        self.compass.text = "W"
                    } else if 281.25 < degrees, degrees <= 303.75 {
                        self.compass.text = "WNW"
                    } else if 303.75 < degrees, degrees <= 326.25 {
                        self.compass.text = "NW"
                    } else if 326.25 < degrees, degrees < 348.75 {
                        self.compass.text = "NNW"
                    }
                   
            }
            }

            // do something in your app
            case let .failure(error):
                // TODO: handle the error == best. comment. ever.
                print(error)
            }

        }
        
    }
    
    
    @IBAction func sharePressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["I am in: " + self.locationLbl.text! + " the temperature is:" + self.weatherLbl.text!.prefix(4)], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        
    }
    

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        lat=locValue.latitude
        lon=locValue.longitude
        moyaCall()
    }
    
    func getLocation() {
        guard CLLocationManager.locationServicesEnabled() else {
            print("pali lokaciju")
            return
        }
        
        let authStatus = CLLocationManager.authorizationStatus()
        guard authStatus == .authorizedWhenInUse else {
            switch authStatus {
            case .denied, .restricted:
                print("nista")
                
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
                
            default:
                print("nema ovdje")
            }
            
            return
        }
    
    
    
}
    


}
