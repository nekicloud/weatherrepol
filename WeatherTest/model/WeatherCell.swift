//
//  WeatherCell.swift
//  WeatherTest
//
//  Created by Luka on 24/02/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit
import Kingfisher

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var forecast: UILabel!
    @IBOutlet weak var degrees: UILabel!

    func updateViews(weather : List){
         let url = URL(string:"http://openweathermap.org/img/w/" + (weather.weather.first?.icon)! + ".png")!
        weatherImage.kf.setImage(with: url)
        let start = weather.dtTxt.index(weather.dtTxt.startIndex, offsetBy: 11)
        let end = weather.dtTxt.index(weather.dtTxt.endIndex, offsetBy: -3)
        let range = start..<end
        currentTime.text = String(weather.dtTxt[range])
        forecast.text = weather.weather.first?.description
        degrees.text = String(format: "%.0f", ((weather.main.temp)-273.15).rounded()) + "°"
    }

}
