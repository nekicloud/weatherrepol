//
//  NetworkService.swift
//  testwe
//
//  Created by Milos Otasevic on 11/02/2019.
//  Copyright © 2019 Milos Otasevic. All rights reserved.
//

import Foundation
import Moya

enum WeatherApp{
    case weather(Double, Double)
}

extension WeatherApp: TargetType {
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    var baseURL: URL { return URL(string: "http://api.openweathermap.org/data/2.5/forecast?APPID=74d05e65fd9872ac7d9444e7b1f5a09f")! }
//    http://api.openweathermap.org/data/2.5/forecast?APPID=74d05e65fd9872ac7d9444e7b1f5a09f&lat=37.785834&lon=-122.406417
    var path: String {
        switch self {
        case .weather:
//            return "&lat=\(latitude)&lon=\(longitude)"
            
            return ""
        }
    }
    var method: Moya.Method {
        switch self {
        case .weather:
            return .get
        }
    }
    
    var task: Task {
        switch self {
         case .weather(let latitude, let longitude):
             var params: [String: Any] = [:]
             params["lat"] = latitude
             params["lon"] = longitude
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        return Data()
        
    }
}

struct Welcome: Codable {
    let cod: String
    let message: Double
    let cnt: Int
    let list: [List]
    let city: City
}

struct City: Codable {
    let id: Int
    let name: String
    let coord: Coord
    let country: String
}

struct Coord: Codable {
    let lat, lon: Double
}

struct List: Codable {
    
    let dt: Int
    let main: MainClass
    let weather: [Weather]
    let clouds: Clouds
    let wind: Wind
    let snow: Rain?
    let sys: Sys
    let dtTxt: String
    let rain: Rain?

    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, snow, sys
        case dtTxt = "dt_txt"
        case rain
    }
    
    let dateFormatter = DateFormatter()

    func getDate() -> Date{
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: dtTxt)!
        return date
        
    }
   
}


struct Clouds: Codable {
    let all: Int
}

struct MainClass: Codable {
    let temp, tempMin, tempMax, pressure: Double
    let seaLevel, grndLevel: Double
    let humidity: Int
    let tempKf: Double
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
        case tempKf = "temp_kf"
    }
}

struct Rain: Codable {
    let the3H: Double?
    
    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}

struct Sys: Codable {
    let pod: Pod
}

enum Pod: String, Codable {
    case d = "d"
    case n = "n"
}

struct Weather: Codable {
    let id: Int
    let main: MainEnum
    let description: String
    let icon: String
}

enum MainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"
    case snow = "Snow"
}

struct Wind: Codable {
    let speed, deg: Double
}
